using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
//public float launchSpeed;
private Rigidbody rigidBody;
public Transform Arrow_Obj;
public float zForce = 500;
	// Use this for initialization
	void Start () {
		

	}

	void Update () {
		
		if(Input.GetMouseButtonDown(0)){
			GetComponent<Rigidbody>().AddRelativeForce(0,0, zForce);
		}
		if(Input.GetKeyDown("space")){
			GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
			Instantiate(Arrow_Obj, transform.position, Arrow_Obj.rotation);

		}
		if(Input.GetKey("a")){
			transform.Rotate(0,-1,0);
		}	
		if(Input.GetKey("d")){
			transform.Rotate(0,1,0);
		}	
		if(Input.GetKey("w")){
			zForce += 5;
		}
	}
}