﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball2 : MonoBehaviour {

	public float speed;
	private Rigidbody rb;
	private int count;
	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		count = 0;
	}

	void FixedUpdate ()
	{
		float moveVertical = Input.GetAxis ("Vertical");

        if (rb.velocity.magnitude == 0){
        
            if (Input.GetKeyDown(KeyCode.W))
            {

                Vector3 movement = new Vector3(0.0f, 0.0f, moveVertical);
                Vector3 relativeMovement = Camera.main.transform.TransformVector(movement);

                rb.AddForce(relativeMovement * speed);
            }
        }
		
	}
	void OnTriggerEnter(Collider other){
		if (other.name == "tile01_hole") {
			StartCoroutine (delayLoad2 ());
		} else if (other.name == "tile25_hole") {
			StartCoroutine (delayLoad3 ());
		} else if (other.name == "tile26_hole") {
			StartCoroutine (delayLoad4 ());
		}

	}
	IEnumerator	delayLoad2(){
		yield return new WaitForSeconds (3); 
		SceneManager.LoadScene ("Hole_2");
	}
	IEnumerator	delayLoad3(){
		yield return new WaitForSeconds (3); 
		SceneManager.LoadScene ("Hole_3");
	}
	IEnumerator	delayLoad4(){
		yield return new WaitForSeconds (3); 
		SceneManager.LoadScene ("Hole_4");
	}
}